import java.util.Scanner;
public class Linear_Search
{
    public static void main(String args[])
    {
        Scanner input=new Scanner(System.in);
        int[] arr = new int[100];
        int i,n,num,pos=-1,found=0;
        System.out.println("Enter the Number of Elements in the Array :");
        n=input.nextInt();
        System.out.println("Enter the Elements in the Array :");
        for(i=0;i<n;i++)
        {
            arr[i]=input.nextInt();
        }
        System.out.println("Enter the Element that has to be Searched :");
        num=input.nextInt();
        for(i=0;i<n;i++)
        {
            if(arr[i]==num)
            {
                found=1;
                pos=i;
                System.out.println(num+" is Found in the Array at Position = "+(pos+1));
                break;
            }
        }
        if(found==0)
        {
            System.out.println(num+" does not Exist in the Array");
        }
    }
}