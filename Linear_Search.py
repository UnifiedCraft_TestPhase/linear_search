arr=[]

found=0

n=int(input("Enter the Number of Elements in the Array : "))

print("Enter the Elements in the Array : ")

for i in range(0,n):
    arr.append(int(input()))

num=int(input("Enter the Element that has to be Searched : "))

for i in range(0,n):
    if num==arr[i]:
        found=1
        pos=i
        print("{} is found in the Array at Position {}".format(num,pos+1))
        break

if found==0:
    print("{} does not exist in the Array".format(num))

